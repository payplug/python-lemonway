===============
python_lemonway
===============

Python API to access Lemonway web services.


Installation
============

Use ``pip``::

    pip install git+https://bitbucket.org/payplug/python-lemonway.git


Contribution
============

Modify ``lemonway/generate_api.py``, then build ``api.py`` with the following command::

  python2 lemonway/generate_api.py

Tests
=====

You can launch tests using::

    python setup.py test

or tox::

    tox
