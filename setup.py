from setuptools import setup
# find_packages is useful to discover tests
from setuptools import find_packages

setup(
    name='python_lemonway',
    version='0.8',
    author='Pierre Pigeau',
    author_email='ppigeau@payplug.com',
    packages=['lemonway'],
    url='',
    license='LICENSE.txt',
    description='',
    long_description=open('README.rst').read(),
    package_data={'lemonway': ['lemonway.wsdl']},
    test_suite='tests',
    install_requires=[
        "suds-jurko==0.6",
        "lxml==3.3.5"
    ],
)
