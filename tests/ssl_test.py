"""
Ensure python-lemonway refuse to send cardholder data if the SSL certificate is not valid
"""
import logging
import sys
import ssl
import unittest
import threading
from lemonway.api import Lemonway
import BaseHTTPServer
import SimpleHTTPServer


location = 'https://localhost:4443/'
wl_login = 'society'
wl_password = '123456'
token = '123467890'
customer_ip = '127.0.0.1'


class TestSSL(unittest.TestCase):
    """
    Ensure the request fails if there are SSL errors
    """

    def setUp(self):
        """
        Runs a https with a self signed certificated listening on port 443
        """
        httpd = BaseHTTPServer.HTTPServer(('localhost', 4443), SimpleHTTPServer.SimpleHTTPRequestHandler)
        httpd.socket = ssl.wrap_socket(httpd.socket, certfile='server.pem', server_side=True)
        thread = threading.Thread(target=httpd.serve_forever)
        thread.daemon = True
        thread.start()

        root = logging.getLogger()
        root.setLevel(logging.INFO)

        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        root.addHandler(ch)

    def test_fail_if_ssl_certificated_is_unvalid(self):
        with self.assertRaises(ssl.SSLError):
            Lemonway(wl_login, wl_password, location).get_balances(None, None)


if __name__ == '__main__':
    unittest.main()
