import logging
import unittest

from lemonway.api import Lemonway
from lemonway.exceptions import LemonwayError
from tests import MockLoggingHandler


class TestLemonwayWsRequestMethod(unittest.TestCase):
    """
    Test the ws_request method of the Lemonway class.
    """

    def setUp(self):
        self.lemonway = Lemonway('some_login', 'some_password', 'a_location')
        # Root logger must be at least at INFO level so that the lemonway logger can emit messages
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)
        logger = logging.getLogger('lemonway')
        self.mock_logging_handler = MockLoggingHandler()
        logger.addHandler(self.mock_logging_handler)

    def test_password_is_not_logged(self):
        parameters = {
            'wlPass': 'the super secret password',
            'some': 'other',
            'parameters': 'for this test'
        }
        with self.assertRaises(LemonwayError):
            # An exception is raised because the method is called with dummy parameters
            self.lemonway.ws_request('test_method', 'test_name', **parameters)
        self.assertGreaterEqual(len(self.mock_logging_handler.messages['info']), 1)
        generated_log = self.mock_logging_handler.messages['info'][0]
        self.assertIn("'some': 'other'", generated_log)
        self.assertIn("'parameters': 'for this test'", generated_log)
        self.assertIn("'wlPass': '***'", generated_log)
        self.assertNotIn('the super secret password', generated_log)
