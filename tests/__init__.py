import logging


class MockLoggingHandler(logging.Handler):
    """
    Mock logging handler to check for expected logs.

    How to use:
    # Declare an instance
    mock_logging_handler = MockLoggingHandler()
    # Add this handler to the logger you want to observe
    some_logger.addHandler(mock_logging_handler)
    # Access the log messages
    mock_logging_handler.messages
    """

    def __init__(self, *args, **kwargs):
        self.messages = {}
        self.reset()
        logging.Handler.__init__(self, *args, **kwargs)

    def emit(self, record):
        self.messages[record.levelname.lower()].append(record.getMessage())

    def reset(self):
        self.messages = {
            'debug': [],
            'info': [],
            'warning': [],
            'error': [],
            'critical': [],
        }
